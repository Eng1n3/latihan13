const http = require("http");
const fs = require("fs");
const port = 8000;

const renderHTML = (path, res) => {
	fs.readFile(path, (err, data) => {
		if (err) {
			res.writeHead(404);
			res.write("Error: page not found");
		} else {
			res.write(data);
		}
		res.end();
	})
}

http
	.createServer((req, res) => {
		res.writeHead(200, {
			'Content-Type': 'text/html'
		});
		const url = req.url;
		switch (url) {
			case '/about':
				res.write("<h1>Ini About</h1>");
				res.end();
				break;
			case '/contact':
				res.write("<h1>Ini Contact</h1>");
				res.end();
				break;
			default:
				renderHTML("./index.html", res);
				break;
		}
	})
	.listen(port, () => {
		console.log(`Server is listening in port 8000...`);
	});
